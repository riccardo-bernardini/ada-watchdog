with Watchdogs.Alarm_Handlers.To_Stderr;
with Watchdogs.Connections;

with Ada.Text_IO; use Ada.Text_IO;
with Ada.Task_Identification;

--
-- This is just a small toy example to illustrate how the watchdog
-- library can be used.  It plays also the role of an informal test.
--
procedure Main is
   use Watchdogs;
   use Watchdogs.Alarm_Handlers;

   --
   -- Get a watcher
   --
   Watcher : constant Connections.Watcher_Type :=
               Connections.Create (Alarm_Handler => new To_Stderr.Handler_Type);

   task type Foo (N : Integer);

   task body Foo is
      Connection : Connections.Watchdog_Connection :=
                     Connections.Open (Watchdog  => Watcher,
                                       Task_Name => "pippo",
                                       ID        => Ada.Task_Identification.Current_Task);

      Sleep_Time : Duration := 0.1 * (if N < 0 then 1.0 else Duration (N));
      Counter : Integer := 0;
   begin
      --
      -- Now we are connected with the watcher that will check that we
      -- call I_Am_Alive regularly
      --
      while N > 0 or else Counter /= -N loop
         --
         -- At every iteration we increase the Sleep_Time so that sooner
         -- or later it will exceed the wake up time of the watcher
         --
         delay Sleep_Time;
         Sleep_Time := Sleep_Time + 0.2;

         Put_Line ("Pippo " & N'Img & " " & Counter'Img); -- Just say something

         --
         -- Tell the watcher we are alive
         --
         Connections.I_Am_Alive (Connection);

         Counter := Counter + 1;
      end loop;
   exception
      when others =>
         Put_Line ("An exception... why?"); -- It should never happen
   end Foo;

   type Foo_Access is access Foo;

   -- Start three tasks of type Foo

   X : Foo_Access := new Foo (1);
   pragma Unreferenced (X);

   Y : Foo_Access := new Foo (2);
   pragma Unreferenced (y);

   Z : Foo_Access := new Foo (-3);
   pragma Unreferenced (Z);

begin
   --
   --  Pretty simple main, uh?  Well, when we arrive here the three tasks
   --  above will be running and the program will not exit until there is
   --  at least a task running (according to Ada rules).
   --
   --  In a normal program we should kill the currently running tasks
   --  and not doing so it would be considered a bug, but in this case
   --  we do it on purpose.  Of course, you'll need to kill it with
   --  a Ctrl-C.
   --
   null;
end Main;
