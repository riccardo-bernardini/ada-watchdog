pragma Ada_2012;
with Ada.Text_IO; use Ada.Text_IO;

package body Watchdogs.Alarm_Handlers.To_Stderr is

   function Format (Name : String; ID : Task_Identification.Task_Id)
                    return String
   is

   begin
      if Name = "" and Id /= Task_Identification.Null_Task_Id then
         return "#" & Task_Identification.Image (Id) & " ";

      elsif Name /= "" and Id = Task_Identification.Null_Task_Id then
         return "'" & Name & "'";

      elsif Name /= "" and Id /= Task_Identification.Null_Task_Id then
         return "'" & Name & "', #" & Task_Identification.Image (Id) & " ";

      else
         raise Constraint_Error;
      end if;
   end Format;
   -----------------
   -- Task_Exited --
   -----------------

   overriding procedure Task_Exited
     (Handler : in out Handler_Type; ID : Task_Identification.Task_Id;
      Name    :        String)
   is
      pragma Unreferenced (Handler);
   begin
      Put_Line (Standard_Error, "[EXITED] Task " & Format (Name, ID));
   end Task_Exited;

   -----------------------
   -- Task_Unresponsive --
   -----------------------

   overriding procedure Task_Unresponsive
     (Handler    : in out Handler_Type;
      ID         : Task_Identification.Task_Id;
      Name       :        String;
      Checkpoint : Checkpoint_Type)
   is
      pragma Unreferenced (Handler);
   begin
      Put_Line (Standard_Error,
                "[UNRESPONSIVE] Task " & Format (Name, ID) &
                  "Last checkpoint : " & Checkpoint_Type'Image (Checkpoint));
   end Task_Unresponsive;

end Watchdogs.Alarm_Handlers.To_Stderr;
