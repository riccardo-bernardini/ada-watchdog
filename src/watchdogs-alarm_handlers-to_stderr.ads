package Watchdogs.Alarm_Handlers.To_Stderr is
   --
   -- Simple alarm handler that writes a message to the standard error
   --
   type Handler_Type is new Alarm_Handler_Interface with private;

   overriding procedure Task_Exited (Handler    : in out Handler_Type;
                                     ID         : Task_Identification.Task_Id;
                                     Name       : String);

   overriding procedure Task_Unresponsive (Handler    : in out Handler_Type;
                                           ID         : Task_Identification.Task_Id;
                                           Name       : String;
                                           Checkpoint : Checkpoint_Type);


private
   type Handler_Type is new Alarm_Handler_Interface with null record;
end Watchdogs.Alarm_Handlers.To_Stderr;
