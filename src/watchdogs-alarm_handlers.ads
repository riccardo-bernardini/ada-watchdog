with Ada.Task_Identification;

use Ada;
package Watchdogs.Alarm_Handlers is
   use type Task_Identification.Task_Id;

   --
   -- Interface for an alarm handler.  Every handler you want to implement
   -- must descend from this and implement Task_Exited and Task_Unresponsive.
   --
   type Alarm_Handler_Interface is limited interface;


   type Alarm_Handler_Access is
     access all Alarm_Handler_Interface'Class;

   --
   -- Called when a task exits.  It receives the task identification
   -- (name and/or ID)
   --
   procedure Task_Exited (Handler    : in out Alarm_Handler_Interface;
                          ID         : Task_Identification.Task_Id;
                          Name       : String)
   is abstract
     with Pre'Class =>
       (Name /= "" or ID /= Task_Identification.Null_Task_Id);

   --
   -- Called when a task is unresponsive.  It receives the task identification
   -- (name and/or ID) and the latest registered checkpoint
   --
   procedure Task_Unresponsive (Handler    : in out Alarm_Handler_Interface;
                                ID         : Task_Identification.Task_Id;
                                Name       : String;
                                Checkpoint : Checkpoint_Type)
   is abstract
     with Pre'Class =>
       (Name /= "" or ID /= Task_Identification.Null_Task_Id);

end Watchdogs.Alarm_Handlers;
