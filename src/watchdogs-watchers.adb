pragma Ada_2012;
--  with Ada.Text_IO; use Ada.Text_IO;
with Ada.Exceptions;
with Ada.Text_IO; use Ada.Text_IO;

package body Watchdogs.Watchers is

   ------------
   -- Create --
   ------------

   function Create (Alarm_Handler : Alarm_Handlers.Alarm_Handler_Access;
                    Sampling      : Duration)
                    return Watchdog_Core
   is
   begin
      return Result : constant Watchdog_Core :=
        Watchdog_Core'(Doa_Table => new Task_Table,
                       Watcher   => new Watchdog_Task,
                       Handler   => Alarm_Handler)
      do
         Result.Watcher.Init (Sampling, Result.Doa_Table, Alarm_Handler);
      end return;
   end Create;

   ----------------
   -- Mark_Alive --
   ----------------

   procedure Mark_Alive (Core       : in out Watchdog_Core;
                         Connection : Connection_Id;
                         Checkpoint : Checkpoint_Type)
   is
   begin
      Core.Doa_Table.Mark_Alive (Connection => Connection,
                                 Checkpoint => Checkpoint);
   end Mark_Alive;

   --------------------
   -- Get_Connection --
   --------------------

   procedure Get_Connection (Core       : in out Watchdog_Core;
                             Task_Name  : String;
                             ID         : Task_Identification.Task_Id;
                             Connection : out Connection_Id)
   is
   begin
      Core.Doa_Table.Get_New_Id (Connection => Connection,
                                 Name       => Task_Name,
                                 ID         => Id);
   end Get_Connection;

   -------------------
   -- Dead_Or_Alive --
   -------------------

   protected body Task_Table is

      -----------
      -- ID_Of --
      -----------

      function ID_Of (Connection : Connection_ID)
                      return Task_Identification.Task_Id
      is
      begin
         if Connection_Table.Contains (Connection) then
            return Connection_Table (Connection).Id;
         else
            raise Program_Error;
         end if;
      end ID_Of;

      -------------
      -- Name_Of --
      -------------

      function Name_Of (Connection : Connection_ID)
                        return String
      is
         use Ada.Strings.Unbounded;
      begin
         if Connection_Table.Contains (Connection) then
            return To_String (Connection_Table (Connection).Name);
         else
            raise Program_Error;
         end if;
      end Name_Of;


      procedure Get_New_Id (Connection : out Connection_ID;
                            Name       : String;
                            ID         : Task_Identification.Task_Id)
      is
         use Ada.Strings.Unbounded;
      begin
         Connection := Next_Id;
         Next_Id := Next_Id + 1;

         Connection_Table.Insert
           (Key      => Connection,
            New_Item => Task_Data'(Name => To_Unbounded_String (Name),
                                   ID   => Id));

         Alive.Insert (Key      => Connection,
                       New_Item => No_Checkpoint);
      end Get_New_Id;

      ----------------
      -- Mark_Alive --
      ----------------

      procedure Mark_Alive
        (Connection : Connection_ID; Checkpoint : Checkpoint_Type)
      is
      begin
         if not Connection_Table.Contains (Connection) then
            -- Mark_Alive has been called with a connection that has
            -- been deleted.  What should we do here?  Ignore it?
            -- raise an exception?
            return;
         end if;

         if Dead.Contains (Connection) then
            Dead.Delete (Connection);
         end if;

         Alive.Include (Key      => Connection,
                        New_Item => Checkpoint);
      end Mark_Alive;

      -----------
      -- Reset --
      -----------

      procedure Reset is
      begin
         Dead.Move (Alive);
      end Reset;

      ------------
      -- Delete --
      ------------

      procedure Delete (Connection : Connection_ID) is
      begin
         if Dead.Contains (Connection) then
            Dead.Delete (Connection);

         elsif Alive.Contains (Connection) then
            Alive.Delete (Connection);

         end if;

         Connection_Table.Delete (Connection);
      end Delete;


      --------------------
      -- Get_Dead_Tasks --
      --------------------

      procedure Get_Dead_Tasks (Set : out Connection_To_Checkpoint_Tables.Map) is
      begin
         Set.Move (Dead);
      end Get_Dead_Tasks;

   end Task_Table;

   -------------------
   -- Watchdog_Task --
   -------------------

   task body Watchdog_Task is
      Task_Table      : Task_Table_Access;
      Alarm_Handler   : Alarm_Handlers.Alarm_Handler_Access;
      Sampling_Period : Duration;

      Dead_Tasks      : Connection_To_Checkpoint_Tables.Map;

      use Connection_To_Checkpoint_Tables;
   begin
      accept Init (Sampling : Duration;
                   Table    : Task_Table_Access;
                   Handler  : Alarm_Handlers.Alarm_Handler_Access)
      do
         Sampling_Period := Sampling;
         Task_Table := Table;
         Alarm_Handler := Handler;
      end Init;

      loop
         delay Sampling_Period;

         Task_Table.Get_Dead_Tasks (Dead_Tasks);

         for Pos in Dead_Tasks.Iterate loop
            declare
               Connection : constant Connection_ID := Key (Pos);
               Checkpoint : constant Checkpoint_Type := Element (Pos);
            begin
               Alarm_Handler.Task_Unresponsive
                 (ID         => Task_Table.Id_Of (Connection),
                  Name       => Task_Table.Name_Of (Connection),
                  Checkpoint => Checkpoint);

               Task_Table.Delete (Connection);
            exception
               when E : others =>
                  Put_Line (Standard_Error,
                            "Exception : " & Ada.Exceptions.Exception_Information (E));
            end;
         end loop;

         Dead_Tasks.Clear;

         Task_Table.Reset;
      end loop;

   end Watchdog_Task;

   -----------
   -- Close --
   -----------

   procedure Close  (Core       : in out Watchdog_Core;
                     Connection : Connection_Id)
   is
   begin
      Core.Handler.Task_Exited
        (ID   => Core.Doa_Table.Id_Of (Connection),
         Name => Core.Doa_Table.Name_Of (Connection));

      Core.Doa_Table.Delete (Connection);
   end Close;


end Watchdogs.Watchers;
