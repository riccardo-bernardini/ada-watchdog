with Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Maps;
with Ada.Task_Identification;


with Watchdogs.Alarm_Handlers;

use Ada;

--
-- This package contains the actual implementation of the watcher.
-- It is private because the final user has no business here
--

private package Watchdogs.Watchers is

   --
   -- "Name" of a connection
   --
   type Connection_Id is private;

   --
   -- Object doing all the work.  This exports an interface similar
   -- to the user visible Watcher_Type.  This object is multitask safe
   -- (with Ada it is just too easy...)
   --
   type Watchdog_Core is limited private;

   function Create (Alarm_Handler : Alarm_Handlers.Alarm_Handler_Access;
                    Sampling      : Duration)
                    return Watchdog_Core;

   procedure Mark_Alive (Core       : in out Watchdog_Core;
                         Connection : Connection_Id;
                         Checkpoint : Checkpoint_Type);

   procedure Get_Connection (Core       : in out Watchdog_Core;
                             Task_Name  : String;
                             ID         : Task_Identification.Task_Id;
                             Connection : out Connection_Id);

   procedure Close  (Core       : in out Watchdog_Core;
                     Connection : Connection_Id);


private

   type Connection_Id is mod 2 ** 16;

   -- Task identification
   type Task_Data is
      record
         Name       : Ada.Strings.Unbounded.Unbounded_String;
         ID         : Task_Identification.Task_Id;
      end record;

   package Connection_To_Task_Tables  is
     new Ada.Containers.Ordered_Maps (Key_Type     => Connection_ID,
                                      Element_Type => Task_Data);

   package Connection_To_Checkpoint_Tables is
     new Ada.Containers.Ordered_Maps (Key_Type     => Connection_ID,
                                      Element_Type => Checkpoint_Type);

   --
   -- The protected object Task_Table is the core data structure.
   -- It keeps which tasks are still alive and which ones did not
   -- confirm that they are alive.
   --
   protected type Task_Table is
      -- Register that the task associated with the connection
      -- just went by th checkpoint
      procedure Mark_Alive (Connection : Connection_ID;
                            Checkpoint : Checkpoint_Type);

      -- Get the set of tasks that did not declared themselves alive
      procedure Get_Dead_Tasks (Set : out Connection_To_Checkpoint_Tables.Map);

      -- Reset the state, setting all task as "to be confirmed alive"
      procedure Reset;

      -- Delete a task
      procedure Delete (Connection : Connection_ID);

      -- Allocate a new connection ID to a task
      procedure Get_New_Id (Connection : out Connection_ID;
                            Name       : String;
                            ID         : Task_Identification.Task_Id);

      function ID_Of (Connection : Connection_ID)
                      return Task_Identification.Task_Id;

      function Name_Of (Connection : Connection_ID)
                        return String;
   private
      --
      -- It works in this way: we keep two sets of "tasks:" Alive (the
      -- tasks that declared to be alive) and Dead (the task that still
      -- have to declare to be alive).  At timeout we read the Dead list
      -- and raise a warning for the tasks in list; successively we copy
      -- (with a Reset) Alive to Dead, restarting the iteration
      --
      Alive   : Connection_To_Checkpoint_Tables.Map;
      Dead    : Connection_To_Checkpoint_Tables.Map;
      Next_Id : Connection_ID := Connection_ID'First;

      -- Keep name and ID of the tasks associated with a connection
      Connection_Table : Connection_To_Task_Tables.Map;
   end Task_Table;

   type Task_Table_Access is access Task_Table;

   task type Watchdog_Task is
      --
      -- This task is the real watchdog: it wakes up every now and then,
      -- check the task table for dead tasks and, if necessary, call
      -- the alarm handler
      --
      entry Init (Sampling : Duration;
                  Table    : Task_Table_Access;
                  Handler  : Alarm_Handlers.Alarm_Handler_Access);
   end Watchdog_Task;

   type Watchdog_Task_Access is access Watchdog_Task;

   --
   -- OK, just glue everything together
   --
   type Watchdog_Core is limited
      record
         Doa_Table : Task_Table_Access;
         Watcher   : Watchdog_Task_Access;
         Handler   : Alarm_Handlers.Alarm_Handler_Access;
      end record;
end Watchdogs.Watchers;
